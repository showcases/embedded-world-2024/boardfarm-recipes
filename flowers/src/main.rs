use std::{
    collections::VecDeque,
    process::{Child, Command},
    thread::sleep,
    time::Duration,
};

fn main() {
    let mut args = std::env::args();
    // Ignore first as that's the binary name
    let _ = args.next();
    let n_flowers = if let Some(arg) = args.next() {
        eprintln!("{arg}");
        arg.parse().unwrap()
    } else {
        4
    };

    let delay = if let Some(arg) = args.next() {
        arg.parse().unwrap()
    } else {
        2
    };
    println!("Will show up to {n_flowers} with {delay}");
    let mut queue = VecDeque::with_capacity(n_flowers + 1);
    loop {
        if queue.capacity() == queue.len() {
            let mut child: Child = queue.pop_front().unwrap();
            let _ = child.kill();
            sleep(Duration::from_secs(delay));
        }
        let f = Command::new("weston-flower").spawn().unwrap();
        queue.push_back(f);
        sleep(Duration::from_secs(delay));
    }
}

